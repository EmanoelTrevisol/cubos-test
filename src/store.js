import Vue from "vue";
import Vuex from "vuex";
import { store as movies } from "./modules/movies";
import { store as common } from "./modules/common";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    movies,
    common
  },
  state: {},
  mutations: {},
  actions: {},
  getters: {},
  strict: process.env.NODE_ENV !== "production"
});

// eslint-disable-next-line
if (module.hot) {
  // eslint-disable-next-line
  module.hot.accept(
    ["@/modules/movies/store", "@/modules/common/store"],
    () => {
      store.hotUpdate({
        modules: {
          movies: require("@/modules/movies/store").default,
          common: require("@/modules/common/store").default
        }
      });
    }
  );
}

export default store;
