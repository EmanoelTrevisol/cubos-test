import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./config/axios";

import VueWait from "vue-wait";

// Custom app wide components
import AppLoader from "./components/AppLoader";
Vue.component("app-loader", AppLoader);

import Pagination from "./components/Pagination";
Vue.component("pagination", Pagination);

import CustomCircledData from "./components/CustomCircledData";
Vue.component("custom-circled-data", CustomCircledData);

import "./registerServiceWorker";

// Stylus
import "./assets/styles/main.styl";
import "./assets/fonts/fonts.styl";

Vue.config.productionTip = false;
Vue.use(VueWait);

new Vue({
  router,
  store,
  wait: new VueWait({ useVuex: true }),
  render: h => h(App)
}).$mount("#app");

import "./config/resolution";
