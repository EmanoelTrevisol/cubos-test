import languages from "./langs";

const tmdb = {
  apiKey: "d258fed0f1beb3f47ffdfbc727c73cc2",
  baseUrl: "https://api.themoviedb.org/3/",
  imageUrls: {
    w300: "https://image.tmdb.org/t/p/w300",
    w500: "https://image.tmdb.org/t/p/w500"
  }
};

export { tmdb };
export { languages };
