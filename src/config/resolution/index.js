import store from "../../store";
import throttle from "lodash/throttle";

const handler = () => {
  const width =
    window.innerWidth ||
    document.documentElement.clientWidth ||
    document.body.clientWidth;

  store.dispatch("common/setWindowWidth", width);
};
handler();

window.addEventListener("resize", throttle(handler, 300));
