import axios from "axios";
import qs from "qs";

import { tmdb } from "@/config";

const { baseUrl, apiKey } = tmdb;

const baseQs = {
  api_key: apiKey,
  language: "pt-BR",
  include_adult: false
};

export default {
  search({ query, page, year }) {
    return axios.get(
      `${baseUrl}search/movie?${qs.stringify({
        ...baseQs,
        query,
        page,
        year
      })}`
    );
  },

  getById(movieId) {
    return axios.get(
      `${baseUrl}movie/${movieId}?${qs.stringify({
        ...baseQs
      })}`
    );
  },

  getVideoByMovieId(movieId) {
    return axios.get(
      `${baseUrl}movie/${movieId}/videos?${qs.stringify({
        ...baseQs
      })}`
    );
  },

  discover({ page }) {
    return axios.get(
      `${baseUrl}discover/movie?${qs.stringify({
        ...baseQs,
        page,
        sort_by: "popularity.desc"
      })}`
    );
  },

  getGenres() {
    return axios.get(
      `${baseUrl}genre/movie/list?${qs.stringify({ ...baseQs })}`
    );
  }
};
