import isEmpty from "lodash/isEmpty";
import { tmdb } from "@/config";

const {
  imageUrls: { w300, w500 }
} = tmdb;

function getGenres({ genres, genreIds, genresArray, isDetail }) {
  if (!isDetail) return genreIds.map(id => genres[id]);

  return genresArray.map(({ name }) => name);
}

function sanitizeMovie({ movie, genres, isDetail = false }) {
  return {
    ...movie,
    genres: getGenres({
      genres,
      genreIds: movie.genre_ids,
      genresArray: movie.genres,
      isDetail
    }),
    releaseDate: new Date(movie.release_date).toLocaleDateString("pt-BR"),
    voteAvgStr: `${movie.vote_average * 10}%`,
    posterUrl: isDetail
      ? `${w500}${movie.poster_path}`
      : `${w300}${movie.poster_path}`
  };
}

export default {
  results({ results, genres }) {
    return !isEmpty(results) && !isEmpty(genres)
      ? results.map(movie => sanitizeMovie({ movie, genres }))
      : [];
  },

  detail({ detail, genres }) {
    return !isEmpty(detail) && !isEmpty(genres)
      ? sanitizeMovie({ movie: detail, genres, isDetail: true })
      : null;
  }
};
