import moviesApi from "../api/movies";
import isEmpty from "lodash/isEmpty";

export default {
  async search({ state, commit, dispatch }) {
    try {
      dispatch("wait/start", "fetching-list", { root: true });
      await dispatch("getGenres");
      const { page, query } = state;

      const year = !Number.isNaN(Number(query)) ? Number(query) : undefined;

      const results = !query
        ? await moviesApi.discover({ page })
        : await moviesApi.search({ page, query, year });

      commit("setResults", results);
    } catch (err) {
      throw err;
    } finally {
      dispatch("wait/end", "fetching-list", { root: true });
    }
  },

  async getGenres({ state, commit }) {
    if (!isEmpty(state.genres)) {
      return;
    }
    const { genres } = await moviesApi.getGenres();
    commit("setGenrers", genres);
  },

  async getMovie({ commit, dispatch }, movieId) {
    try {
      dispatch("wait/start", "fetching-movie", { root: true });
      await dispatch("getGenres");

      const [movie, video] = await Promise.all([
        moviesApi.getById(movieId),
        moviesApi.getVideoByMovieId(movieId)
      ]);

      commit("setDetailMovie", { ...movie, video });
    } catch (err) {
      throw err;
    } finally {
      dispatch("wait/end", "fetching-movie", { root: true });
    }
  },

  setFilter({ commit }, filter) {
    commit("setFilter", filter);
  }
};
