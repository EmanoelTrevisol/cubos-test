import Vue from "vue";

export default {
  setResults(state, result) {
    Object.entries(result).forEach(([key, value]) => {
      Vue.set(state, key, value);
    });
  },

  setGenrers(state, genresArr) {
    genresArr.forEach(({ id, name }) => {
      Vue.set(state.genres, id, name);
    });
  },

  setDetailMovie(state, movie) {
    Vue.set(state, "detail", movie);
  },

  setFilter(state, { key, value }) {
    Vue.set(state, key, value);
  }
};
