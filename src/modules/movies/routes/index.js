const List = () => import(/* webpackChunkName: 'movies' */ "../views/List.vue");
const Detail = () =>
  import(/* webpackChunkName: 'movies' */ "../views/Detail.vue");

export default [
  {
    path: "/",
    name: "movie-list",
    component: List
  },
  {
    path: "/detalhe/:movieId",
    name: "movie-detail",
    component: Detail,
    props: true
  }
];
