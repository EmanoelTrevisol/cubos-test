import Vue from "vue";

export default {
  setWindowWidth(state, value) {
    Vue.set(state, "windowWidth", value);
  }
};
