export default {
  setWindowWidth({ commit }, value) {
    commit("setWindowWidth", value);
  }
};
