import actions from "./commonActions";
import mutations from "./commonMutations";
import state from "./commonState";

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
