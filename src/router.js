import Vue from "vue";
import Router from "vue-router";
import { routes as moviesRoutes } from "./modules/movies";

Vue.use(Router);

// const fs = require('fs');
export default new Router({
  mode: "history",
  routes: [...moviesRoutes]
});
